package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String parameter = request.getParameter("id");

        if (StringUtils.isBlank(parameter) || !parameter.matches("^[0-9]*$")) {
        	errorMessages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
        	return;
        }

	    int id = Integer.parseInt(parameter);
	    Message message = new Message();
	    message.setId(id);

	    Message editMessage = new MessageService().select(id);

	    if (editMessage == null) {
	    	errorMessages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
        	return;
	    }

        request.setAttribute("message", editMessage);
        request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        User user = (User) session.getAttribute("loginUser");

        Message message = new Message();

        message.setId(Integer.parseInt(request.getParameter("id")));
        message.setText(request.getParameter("text"));
        message.setUserId(user.getId());

        if (isValid(message, errorMessages)) {
            try {
            	new MessageService().update(message);
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        }

        if (errorMessages.size() != 0) {
        	request.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("message", message);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }

        request.setAttribute("message", message);
        response.sendRedirect("./");

	 }

	private boolean isValid(Message message, List<String> errorMessages) {

		String text = message.getText();

        if (StringUtils.isBlank(text)) {
            errorMessages.add("入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }

        return true;
    }

}
