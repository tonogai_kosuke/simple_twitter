package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {

    private int id;
    private String account;
    private String name;
    private int userId;
    private String text;
    private Date createdDate;

    
    public void setId(int id) {
    	this.id = id;
    }

    public int getId() {
    	return id;
    }

    public void setAccount(String account) {
    	this.account = account;
    }

    public String getAccount() {
    	return account;
    }

    public void setName(String name) {
    	this.name = name;
    }

    public String getName() {
    	return name;
    }

    public void setUserId(int userId) {
    	this.userId = userId;
    }

    public int getUserId() {
    	return userId;
    }

    public void setText(String text) {
    	this.text = text;
    }

    public String getText() {
    	return text;
    }
    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
    	return createdDate;
    }
}